import { Component, OnInit } from '@angular/core';
import { SlimScrollOptions } from 'ng2-slimscroll';

@Component({
  selector: 'app-sentiment',
  template:`
<div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; ">
  <span style=" display: inline-block; padding: 0.25em 0.4em;font-size: 75%;font-weight: bold;line-height: 1;color: #fff;text-align: center;white-space: nowrap; vertical-align: baseline;  background-color: #20a8d8;"> Positive</span>
    <div style=" height: 305px;display: block;">
      <div style="padding-left: 0; margin-bottom: 0;padding-right: 0px;background: white; margin: 0; height: 100%; overflow-y: auto; " slimScroll [options]="options">
          <div *ngFor="let data of itemsPos"> 
            <span style=" position: relative;display: block;padding: 0.75rem 1.25rem; margin-bottom: -1px;background-color: #fff; border: 1px solid #ddd; margin-bottom: 0; ">
              <div style=" cursor: pointer; padding-left: 20px; color: #666;">
                  <img src="{{data.image}}" alt="{{data.media_displayName}}" class="profile-picture" onerror="this.style.display='none'">
                      <strong>{{data.screen_name}}</strong>
                          <span style="color: #666;"> {{data.screen_name}}</span>  
                           <a target="_blank"style="color: #666; white-space: nowrap;font-size: .9em;">{{data.timestamp }}</a>
                        <p>
                        {{data.desc}}  
                        </p>
                         <span>{{data.text}}</span>
              </div>
            </span>
          </div>
        </div>
    </div>
</div>

<div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; ">
  <span style="display: inline-block; padding: 0.25em 0.4em;font-size: 75%;font-weight: bold;line-height: 1;color: #fff;text-align: center;white-space: nowrap; vertical-align: baseline;  background-color: #93a6af;"> Neutral</span>
     <div style=" height: 305px;display: block;">
      <div style=" padding-left: 0; margin-bottom: 0;padding-right: 0px;background: white; margin: 0; height: 100%; overflow-y: auto; " slimScroll [options]="options">
          <div *ngFor="let data of itemsNeu"> 
            <span style=" position: relative;display: block;padding: 0.75rem 1.25rem; margin-bottom: -1px;background-color: #fff; border: 1px solid #ddd; margin-bottom: 0; ">
              <div style=" cursor: pointer; padding-left: 20px; color: #666;">
                  <img src="{{data.image}}" alt="{{data.media_displayName}}" style=" width: 48px;height: 48px;" onerror="this.style.display='none'">
                      <strong>{{data.screen_name}}</strong>
                          <span style="color: #666;" > {{data.screen_name}}</span>  
                           <a target="_blank" style="color: #666; white-space: nowrap;font-size: .9em;">{{data.timestamp }}</a>
                        <p>
                        {{data.desc}}  
                        </p>
                         <span>{{data.text}}</span>
              </div>
            </span>
          </div>
      </div>
    </div>
</div>


<div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; ">
  <span style="display: inline-block; padding: 0.25em 0.4em;font-size: 75%;font-weight: bold;line-height: 1;color: #fff;text-align: center;white-space: nowrap; vertical-align: baseline;  background-color: #f86c6b;"> Negative</span>
     <div style=" height: 305px;display: block;">
      <div style="padding-left: 0; margin-bottom: 0;padding-right: 0px;background: white; margin: 0; height: 100%; overflow-y: auto; " slimScroll [options]="options">
          <div *ngFor="let data of itemsNeg"> 
            <span style=" position: relative;display: block;padding: 0.75rem 1.25rem; margin-bottom: -1px;background-color: #fff; border: 1px solid #ddd; margin-bottom: 0; ">
              <div style=" cursor: pointer; padding-left: 20px; color: #666;">
                  <img src="{{data.image}}" alt="{{data.media_displayName}}" class="profile-picture" onerror="this.style.display='none'">
                      <strong>{{data.screen_name}}</strong>
                          <span style="color: #666;" > {{data.screen_name}}</span>  
                           <a target="_blank" style="color: #666; white-space: nowrap;font-size: .9em;">{{data.timestamp }}</a>
                        <p>
                        {{data.desc}}  
                        </p>
                         <span>{{data.text}}</span>
              </div>
            </span>
          </div>
      </div>
    </div>
</div>`,
  styles: ['tag {display: inline-block;padding: 0.25em 0.4em;font-size: 75%;font-weight: bold;line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; } background-color: #20a8d8; background-color: #b0bec5; background-color: #f86c6b;streaming-news-timeline{height: 305px;display: block;}.list-news .list-group-item{padding: 10px 0; background: transparent; border-left: none; border-right: none; border-color: #ddd; font-size: 13px;cursor: pointer; content-news:hover; content-news.disabled:focus; contents-news.disabled:hover{background-color: #000;}}']
})
export class BaseWidgetSentimentComponent implements OnInit {

  options: SlimScrollOptions; 
  private itemsPos: any;
  private itemsNeu: any;
  private itemsNeg: any;
  public sentiment = {
       "pos": {
         "items1":[
                   {
                  "tweet_id": "856167623356104705",
                  "created_date": "Sun Apr 23 22:27:22 WIB 2017",
                  "user_id": "45843545",
                  "name": "\\w/,",
                  "screen_name": "WawanPapAiko",
                  "image": "http://pbs.twimg.com/profile_images/855642238474371072/6qFK4DH8_normal.jpg",
                  "tweet_text": "@kompascom @yunartowijaya Apa cuma gue yg merasa @jokowi 🙈🙉🙊 tunggu waktu atau pura2 tidak tahu..?\nPercuma infrastu… https://t.co/0qnRUaq2GC",
                  "sentiment": "0",
                  "timestamp": 1492961242000,
                  "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                  },
                  {
                  "tweet_id": "856167043489345536",
                  "created_date": "Sun Apr 23 22:25:04 WIB 2017",
                  "user_id": "316313045",
                  "name": "banyubenning",
                  "screen_name": "adoncrush",
                  "image": "http://pbs.twimg.com/profile_images/832207924210475008/5Bws2d8K_normal.jpg",
                  "tweet_text": "RT @kompascom: Khofifah: Kelompok Anti-Pancasila Bergerak Melalui Forum Kajian Agama. https://t.co/Wn66iPvFJd",
                  "sentiment": "0",
                  "timestamp": 1492961104000,
                  "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                  },
                  {
                  "tweet_id": "856097899419025408",
                  "created_date": "Sun Apr 23 17:50:19 WIB 2017",
                  "user_id": "781521757559672833",
                  "name": "Iwel 69",
                  "screen_name": "IwelSanjaya",
                  "image": "http://pbs.twimg.com/profile_images/854310009353535488/h25Jm8y6_normal.jpg",
                  "tweet_text": "RT @kompascom: Sandiaga: Gaji Saya sebagai Wagub Akan Disumbangkan ke Kaum Dhuafa. https://t.co/KTTL06LyHx",
                  "sentiment": "0",
                  "timestamp": 1492944619000,
                  "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                  },
                  {
                  "tweet_id": "856097516361629696",
                  "created_date": "Sun Apr 23 17:48:48 WIB 2017",
                  "user_id": "2277291085",
                  "name": "Femi",
                  "screen_name": "Femmy_CR",
                  "image": "http://pbs.twimg.com/profile_images/855386747147919361/7sX6VnKl_normal.jpg",
                  "tweet_text": "RT @MosaToba: @kompascom Para Wajah Beringas Bersorban kontradiksi dg ajakan @aniesbaswedan utk bawa Pesan Damai.",
                  "sentiment": "0",
                  "timestamp": 1492944528000,
                  "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                  }
              ]
          },

    
      "neu":{
         "items1":[
                    {
                    "tweet_id": "856444244742950912",
                    "created_date": "Mon Apr 24 16:46:34 WIB 2017",
                    "user_id": "2453575590",
                    "name": "ابدل النحد",
                    "screen_name": "OdyiusJacare",
                    "image": "http://pbs.twimg.com/profile_images/855445737693298688/KzjrV8VD_normal.jpg",
                    "tweet_text": "RT @SuradjiHosni: @kompascom SBY yg menjadikan WaPres disebut ZALIM.\nJangan heran kelak giliran @jokowi  yg disebut ZALIM.",
                    "sentiment": "0",
                    "timestamp": 1493027194000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },
                    {
                    "tweet_id": "856443849694040064",
                    "created_date": "Mon Apr 24 16:45:00 WIB 2017",
                    "user_id": "462670954",
                    "name": "LhoMuslimKokEmosian?",
                    "screen_name": "UcengPilek",
                    "image": "http://pbs.twimg.com/profile_images/855066007978754052/y1vzJ8VG_normal.jpg",
                    "tweet_text": "RT @SuradjiHosni: @kompascom SBY yg menjadikan WaPres disebut ZALIM.\nJangan heran kelak giliran @jokowi  yg disebut ZALIM.",
                    "sentiment": "0",
                    "timestamp": 1493027100000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },
                    {
                    "tweet_id": "856443195646820353",
                    "created_date": "Mon Apr 24 16:42:24 WIB 2017",
                    "user_id": "1517748600",
                    "name": "#vicar_dust ✌",
                    "screen_name": "Kardusnyiromlah",
                    "image": "http://pbs.twimg.com/profile_images/799245740673372161/PR6h1kpY_normal.jpg",
                    "tweet_text": "Bagaimana  ini pak.....@basuki_btp @WagubDKI @boyrafliamar @DivHumasPolri @DKIJakarta @Dishubtrans_DKI @kompascom… https://t.co/PeWxNskoNo",
                    "sentiment": "0",
                    "timestamp": 1493026944000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },
                    {
                    "tweet_id": "856440791937335297",
                    "created_date": "Mon Apr 24 16:32:51 WIB 2017",
                    "user_id": "758679576499191808",
                    "name": "mashadi",
                    "screen_name": "mashadi421",
                    "image": "http://pbs.twimg.com/profile_images/854688420672684034/mqM_SLGa_normal.jpg",
                    "tweet_text": "@kompascom Janji yg umroh ada juga yg ke Roma dan Yerusalem ini perintah big boss @PartaiSocmed buzzernya kebagian… https://t.co/AmFtww9zP8",
                    "sentiment": "0",
                    "timestamp": 1493026371000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },

               ]
      },

      "neg": {
         "items1":[
                   {
                    "tweet_id": "856846529843351552",
                    "created_date": "Tue Apr 25 19:25:06 WIB 2017",
                    "user_id": "619035146",
                    "name": "Cindy Chandra",
                    "screen_name": "gcindych",
                    "image": "http://pbs.twimg.com/profile_images/743670048527548420/-egekhf8_normal.jpg",
                    "tweet_text": "RT @kompascom: Banjir Karangan Bunga untuk Ahok dan Djarot di Balai Kota DKI. https://t.co/9tzzH2N4EH",
                    "sentiment": "0",
                    "timestamp": 1493123106000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },
                    {
                    "tweet_id": "856823254681493504",
                    "created_date": "Tue Apr 25 17:52:37 WIB 2017",
                    "user_id": "4398387972",
                    "name": "Michael 76N",
                    "screen_name": "michaellaen84",
                    "image": "http://pbs.twimg.com/profile_images/778037062867968000/THYY4udi_normal.jpg",
                    "tweet_text": "@ZaangGo @kompascom nyari simpati agar dipilih? pilkada udah selese cuy... nyari simpati agar apa?",
                    "sentiment": "-1",
                    "timestamp": 1493117557000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },
                    {
                    "tweet_id": "856816580608393216",
                    "created_date": "Tue Apr 25 17:26:06 WIB 2017",
                    "user_id": "871412892",
                    "name": "firmansyah adi",
                    "screen_name": "PHNCX",
                    "image": "http://pbs.twimg.com/profile_images/775075136923774977/I2DNnMib_normal.jpg",
                    "tweet_text": "RT @kompascom: \"Terima Kasih, Pak Jokowi!\". https://t.co/3i1OAZGQYF",
                    "sentiment": "0",
                    "timestamp": 1493115966000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },
                    {
                    "tweet_id": "856816401834815488",
                    "created_date": "Tue Apr 25 17:25:23 WIB 2017",
                    "user_id": "2376944491",
                    "name": "Smart Santa",
                    "screen_name": "SmartSanta",
                    "image": "http://pbs.twimg.com/profile_images/605053791889686528/XT0dbCZN_normal.jpg",
                    "tweet_text": "@jakpost @thejakartaglobe @kompascom @jawapos @tempodotco @detikcom @Metro_TV @KickAndyShow @OfficialRCTI @SCTV_… https://t.co/OiYLdLILx0",
                    "sentiment": "0",
                    "timestamp": 1493115923000,
                    "text": "About two months after President Donald Trump took the oath of office, a key position in the State Department remains unfilled. The Trump administration has not yet appointed a spokesperson, ...",
                    },
      ],
      },
      
    };
  

  constructor() {  
          this.options = {
          barBackground: '#656a6f',
          gridBackground: '#e1e1e1',
          barWidth: '5',
          gridWidth: '2'
        };
      }

  ngOnInit() {
    this.itemsPos = this.sentiment['pos'].items1;
    this.itemsNeu = this.sentiment['neu'].items1;
    this.itemsNeg = this.sentiment['neg'].items1;

  }

}

