import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SlimScrollModule } from 'ng2-slimscroll';
import { AppComponent } from './app.component';
import { DemoSentimentComponent } from './demo-sentiment/demo-sentiment.component';
import { SentimentModule } from '../../../index';

@NgModule({
  declarations: [
    AppComponent,
    DemoSentimentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SlimScrollModule,
    SentimentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
