import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSentimentComponent } from './demo-sentiment.component';

describe('DemoSentimentComponent', () => {
  let component: DemoSentimentComponent;
  let fixture: ComponentFixture<DemoSentimentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSentimentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSentimentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
