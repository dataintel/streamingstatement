import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetSentimentComponent } from './src/sentiment.component';
import { SlimScrollModule } from 'ng2-slimscroll';
export * from './src/sentiment.component';

@NgModule({
  imports: [
    CommonModule,
    SlimScrollModule
  ],
  declarations: [
    BaseWidgetSentimentComponent,
  ],
  exports: [
    BaseWidgetSentimentComponent,
  ]
})
export class SentimentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SentimentModule,
      providers: []
    };
  }
}
